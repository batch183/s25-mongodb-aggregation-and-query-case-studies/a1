// FOR CONTEXT INCLUDED DATA
/*

	db.fruits.insertMany([
	{
		"name":"Apple",
		"color":"Red",
		"supplier_id":1,
		"stocks":20,
		"price":40,
		"onSale": true,
		"origin": ["Philippines","US"]
	},
	{
		"name":"Banana",
		"color":"Yellow",
		"supplier_id":2,
		"stocks":15,
		"price":20,
		"onSale": true,
		"origin": ["Philippines","Ecuador"]
	},
	{
		"name":"Kiwi",
		"color":"Green",
		"supplier_id":1,
		"stocks":25,
		"price":50,
		"onSale": true,
		"origin": ["China","US"]
	},
	{
		"name":"Mango",
		"color":"Yellow",
		"supplier_id":2,
		"stocks":10,
		"price":120,
		"onSale": false,
		"origin": ["Philippines","India"]
	}
])

*/


// 2. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$onSale", "fruitsOnSale": {$sum: 1}}},
	{$project: {"_id": 0}}
]);

//3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
	{$match: {"stocks": {$gte:20}}},
	{$group: {"_id": "$OnStock", "enoughStock": {$sum: 1}}},
	{$project: {"_id": 0}}
]);

// 4. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "avg_price": {$avg: "$price"}}}
]);

// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "max_price": {$max: "$price"}}}
]);

// 6. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "min_price": {$min: "$price"}}}
]);